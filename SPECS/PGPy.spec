Name:           PGPy
Version:        0.5.4
Release:        1%{dist}
Summary:        Pretty Good Privacy for Python

License:        BSD
URL:            https://github.com/SecurityInnovation/PGPy
Source0:        https://github.com/SecurityInnovation/PGPy/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python3-devel

BuildRequires:  python3dist(cryptography)
BuildRequires:  python3dist(pyasn1)
BuildRequires:  python3dist(six)
BuildRequires:  python3dist(singledispatch)
BuildRequires:  python3dist(wheel)

%description
PGPy is a Python (2 and 3) library for implementing Pretty Good Privacy into Python programs, conforming to the OpenPGP specification per RFC 4880.

%package     -n python3-pgpy
Summary:        Pretty Good Privacy for Python3
Requires:       python3dist(cryptography)
Requires:       python3dist(pyasn1)
Requires:       python3dist(six)
Requires:       python3dist(singledispatch)

%description -n python3-pgpy
PGPy is a Python (2 and 3) library for implementing Pretty Good Privacy into Python programs, conforming to the OpenPGP specification per RFC 4880.

This package contains Python 3 version.


%prep
%autosetup
cp -a . ../python3

%build
pushd ../python3
%{py3_build}
popd


%install
pushd ../python3
%{py3_install}
popd


%check
#FIXME investigate all failures
pushd ../python3
%{__python3} -m pytest ||:
popd


%files -n python3-pgpy
%license LICENSE
%doc README.rst
%{python3_sitelib}/*


%changelog
